import random
import pytest
from faker import Faker
import requests


def special_middle_letter(s):
    return(s[:len(s)//2] + random.choice("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ") + s[len(s)//2:])


url = "http://192.168.0.16:8080/students/general"
fake = Faker(locale="ru_RU")
input_data = [{"lastname": j[0], "firstname": j[1], "patronymic": j[2]} for j in [fake.name().split() for i in range(14)]]
# print(input_data) Формат данных: массив словарей, где ключи имя, отчество, фамилия
# а данные представлены в виде "Иван" "Иванов" "Иванович" соответственно, длина всех строк точно больше 3
test_json = []
expected_status_codes = []
expected_body_response = []
# Вручную заносим определённые данные для тестов
# Данные 1 : всё корректно - все поля заполнены как надо
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[0]["firstname"]}',
            "lastname": f'{input_data[0]["lastname"]}',
            "patronymic": f'{input_data[0]["patronymic"]}'
        }
    }
})
expected_status_codes.append(200)

# Данные 2 : всё корректно - все поля заполнены как надо, отчества нет
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[1]["firstname"]}',
            "lastname": f'{input_data[1]["lastname"]}'
        }
    }
})
expected_status_codes.append(200)

# Данные 3 : некорректно - нету имени
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": "",
            "lastname": f'{input_data[2]["lastname"]}',
            "patronymic": f'{input_data[2]["patronymic"]}'
        }
    }
})
expected_status_codes.append(202)

# Данные 4 : некорректно - нету фамилии
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[3]["firstname"]}',
            "lastname": "",
            "patronymic": f'{input_data[3]["patronymic"]}'
        }
    }
})
expected_status_codes.append(202)

# Данные 5 : всё корректно - все данные с маленькой буквы
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[4]["firstname"].lower()}',
            "lastname": f'{input_data[4]["lastname"].lower()}',
            "patronymic": f'{input_data[4]["patronymic"].lower()}'
        }
    }
})
expected_status_codes.append(200)

# Данные 6 : некорректно - все данные только заглавные буквы
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[5]["firstname"].upper()}',
            "lastname": f'{input_data[5]["lastname"].upper()}',
            "patronymic": f'{input_data[5]["patronymic"].upper()}'
        }
    }
})
expected_status_codes.append(202)
# Данные 7 : некорректно - имя целиком из заглавных букв
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[6]["firstname"].upper()}',
            "lastname": f'{input_data[6]["lastname"]}',
            "patronymic": f'{input_data[6]["patronymic"]}'
        }
    }
})
expected_status_codes.append(202)

# Данные 8 : некорректно - фамилия только заглавные буквы
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[7]["firstname"]}',
            "lastname": f'{input_data[7]["lastname"].upper()}',
            "patronymic": f'{input_data[7]["patronymic"]}'
        }
    }
})
expected_status_codes.append(202)

# Данные 9 : некорректно - отчество только заглавные буквы
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[8]["firstname"]}',
            "lastname": f'{input_data[8]["lastname"]}',
            "patronymic": f'{input_data[8]["patronymic"].upper()}'
        }
    }
})
expected_status_codes.append(202)

# Данные 10 : некорректно - двойное имя
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[9]["firstname"]+"-"+input_data[9]["firstname"]}',
            "lastname": f'{input_data[9]["lastname"]}',
            "patronymic": f'{input_data[9]["patronymic"]}'
        }
    }
})
expected_status_codes.append(202)

# Данные 11 : некорректно - двойная фамилия
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[10]["firstname"]}',
            "lastname": f'{input_data[10]["lastname"]+"-"+input_data[10]["lastname"]}',
            "patronymic": f'{input_data[10]["patronymic"]}'
        }
    }
})
expected_status_codes.append(202)

# Данные 12 : некорректно - в середине имени есть заглавная буква
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{special_middle_letter(input_data[11]["firstname"])}',
            "lastname": f'{input_data[11]["lastname"]}',
            "patronymic": f'{input_data[11]["patronymic"]}'
        }
    }
})
expected_status_codes.append(202)

# Данные 13 : некорректно - в середине фамилии есть заглавная буква
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{input_data[12]["firstname"]}',
            "lastname": f'{special_middle_letter(input_data[12]["lastname"])}',
            "patronymic": f'{input_data[12]["patronymic"]}'
        }
    }
})
expected_status_codes.append(202)

# Данные 14 : некорректно - фигню какую-то ввели в имя
test_json.append(
{
    "student": {
        "personalData": {
            "firstname": f'{fake.postcode()}',
            "lastname": f'{input_data[13]["lastname"]}',
            "patronymic": f'{input_data[13]["patronymic"]}'
        }
    }
})
expected_status_codes.append(202)


def test_correct_all_data():
    # Данные 1 : всё корректно - все поля заполнены как надо
    response = requests.post(url, json=test_json[0])
    assert response.status_code == expected_status_codes[0]
    answer = response.json()
    assert answer["code"] == "Ok"
    assert test_json[0]["student"]["personalData"]["lastname"] == answer["info"][0]["personalData"]["lastname"]
    assert test_json[0]["student"]["personalData"]["firstname"] == answer["info"][0]["personalData"]["firstname"]
    assert test_json[0]["student"]["personalData"]["patronymic"] == answer["info"][0]["personalData"]["patronymic"]

def test_correct_data_without_patronymic():
    # Данные 2 : всё корректно - все поля заполнены как надо, отчества нет
    response = requests.post(url, json=test_json[1])
    assert response.status_code == expected_status_codes[1]
    answer = response.json()
    assert answer["code"] == "Ok"
    assert test_json[1]["student"]["personalData"]["lastname"] == answer["info"][0]["personalData"]["lastname"]
    assert test_json[1]["student"]["personalData"]["firstname"] == answer["info"][0]["personalData"]["firstname"]

def test_no_firstname():
    # Данные 3 : некорректно - нету имени
    response = requests.post(url, json=test_json[2])
    assert response.status_code == expected_status_codes[2]
    answer = response.json()
    assert answer["code"] == "StrErr"
    assert answer["errorFields"][0]["field"] == "student.personalData.firstname"


def test_no_lastname():
    # Данные 4 : некорректно - нету фамилии
    response = requests.post(url, json=test_json[3])
    assert response.status_code == expected_status_codes[3]
    answer = response.json()
    assert answer["code"] == "StrErr"
    assert answer["errorFields"][0]["field"] == "student.personalData.lastname"

def test_all_data_lower():
    # Данные 5 : всё корректно - все данные с маленькой буквы
    response = requests.post(url, json=test_json[4])
    assert response.status_code == expected_status_codes[4]
    answer = response.json()
    assert answer["code"] == "Ok"
    assert test_json[4]["student"]["personalData"]["lastname"] == answer["info"][0]["personalData"]["lastname"]
    assert test_json[4]["student"]["personalData"]["firstname"] == answer["info"][0]["personalData"]["firstname"]
    assert test_json[4]["student"]["personalData"]["patronymic"] == answer["info"][0]["personalData"]["patronymic"]

def test_all_data_upper():
    # Данные 6 : некорректно - все данные только заглавные буквы
    response = requests.post(url, json=test_json[5])
    assert response.status_code == expected_status_codes[5]
    answer = response.json()
    assert answer["code"] == "StrErr"
    assert answer["errorFields"][0]["field"] == "student.personalData.firstname"
    assert answer["errorFields"][1]["field"] == "student.personalData.lastname"
    assert answer["errorFields"][2]["field"] == "student.personalData.patronymic"